const browserZoomFactorParams = () => {
  return {globalThis};
};

export const browserZoomFactor = ({globalThis} = browserZoomFactorParams()) => {
  if ('devicePixelRatio' in globalThis) {
    const devicePixelRatio = globalThis.devicePixelRatio;
    if (globalThis.screen && globalThis.screen.scaleFactor) {
      const scaleFactor = globalThis.screen.scaleFactor;
      return devicePixelRatio / scaleFactor;
    }
  }
  return null;
};

const pinchZoomFactorParams = () => {
  return {globalThis};
};

export const pinchZoomFactor = ({globalThis} = pinchZoomFactorParams()) => {
  if ('visualViewport' in globalThis && globalThis.visualViewport.scale) {
    return globalThis.visualViewport.scale;
  }
  return null;
}
