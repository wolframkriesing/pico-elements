# pico-elements
These are web components (element) mostly used on [picostitch.com](https://picostitch.com)
to enhance the web site and content,
see [the examples](https://wolframkriesing.codeberg.page/pico-elements/examples/).

The element [`<pico-viewport-stats>`](https://wolframkriesing.codeberg.page/pico-elements/examples/)
shows some viewport statistics, which I found useful to show on picostitch, to 
understand and visaulize the lost space on a website, especially when shown on 
big screens.

## Use a pico-element on Your Site
You can embed a pico-element on any site. I am not giving any guarantee for it
to continue to work at any time though.

The most convinient way is looking at the [examples](https://wolframkriesing.codeberg.page/pico-elements/examples/)
and inspect the code and maybe play around with it, this should be much most
explanatory.

### Use `<pico-viewport-stats>`
Add the following code to your HTML page:
```
<!-- Load the web component. This will only work in browsers supporting ES Modules. -->
<script type="module" src="https://wolframkriesing.codeberg.page/pico-elements/pico-viewport-stats.js"></script>

<!-- Render the viewport stats. -->
<pico-viewport-stats></pico-viewport-stats>

<!-- 
    if you just want to see all detailed info as a JSON, you can add the attribute "debug",
    but I am assume this is not really useful too often. 
-->
<pico-viewport-stats debug></pico-viewport-stats>
```

### Use `<pico-image-carousel>`
To create a simple image carousel on your site
add the following code to your HTML page:
```
<!-- Load the web component. This will only work in browsers supporting ES Modules. -->
<script type="module" src="https://wolframkriesing.codeberg.page/pico-elements/pico-image-carousel.js"></script>

<!-- Render an image carousel with three images. -->
<pico-image-carousel>
  <img src="image1.webp">
  <img src="image2.avif">
  <img src="image3.jpg">
</pico-image-carousel>
```

## Develop (run locally)

This project uses docker to provide the environment needed to run this site.
This way only docker and docker-compose are required for running this project locally.

```
> docker-compose up -d
Creating network "pico-elements_default" with the default driver
Creating pico-elements ... done
> docker exec -it pico-elements bash # enter the container
root@12345:/app# npm install 
root@12345:/app# npm run dev:start # start the local dev server, serving the files from `src`
...

Starting up http-server, serving _output
Available on:
  http://127.0.0.1:5011
  http://172.1.2.3:5011
Hit CTRL-C to stop the server
```

Now you should be able to open [localhost:5011/src/examples](http://localhost:5011/src/examples) and see the examples page.

## npm commands

- `npm run start` start a server and build on demand
- `npm run test:watch` run tests continuously and watch for changes which automatically rerun the tests
- `npm run build` to build the site
- `npm run test` run the tests
